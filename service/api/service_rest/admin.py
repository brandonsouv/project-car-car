from django.contrib import admin
from .models import AutomobilesVO, Technician, Appointment


@admin.register(AutomobilesVO)
class ServiceAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "vin",
    )


@admin.register(Technician)
class ServiceAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
        "employee_number",
    )


@admin.register(Appointment)
class ServiceAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "customer_name",
        "vin",
        "reason",
        "datetime",
        "technician",
        "is_vip",
        "is_completed",
    )
