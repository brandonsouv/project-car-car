# Generated by Django 4.0.3 on 2023-03-09 07:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_remove_automobilevo_color_remove_automobilevo_year_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salerecord',
            name='automobile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sale_record', to='sales_rest.automobilevo'),
        ),
        migrations.AlterField(
            model_name='salerecord',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sale_record', to='sales_rest.customer'),
        ),
        migrations.AlterField(
            model_name='salerecord',
            name='sales_person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sale_record', to='sales_rest.salesperson'),
        ),
    ]
