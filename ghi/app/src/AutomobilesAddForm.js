import React, {useState} from 'react';
import { useNavigate } from 'react-router-dom';


function AutomobilesAddForm({getAutomobilesData, vehicleModels}) {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVIN] = useState('');
  const [model, setModel] = useState('');
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }
  const handleYearChange = (event) => {
    const value = event.target.value;
    setYear(value);
  }
  const handleVINChange = (event) => {
    const value = event.target.value;
    setVIN(value);
  }
  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  }
  const navigate = useNavigate();
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = model;
    const url = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      getAutomobilesData()
      navigate('/automobiles');
    }
  }
  return(
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={handleSubmit} id="create-automobiles-form">

              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleYearChange} value={year} placeholder="year" required type="text" name="year" id="year" className="form-control align-middle" />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleVINChange} value={vin} placeholder="vin" required type="text" name="vin" maxLength="17" id="pictureUrl" className="form-control align-middle" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select onChange={handleModelChange} value={model} required id="model" name="model" className="form-select">
                  <option value="">Choose a model</option>
                  {vehicleModels.map((model) => {
                          return (
                            <option value={model.id} key={model.id}>
                              {model.name}
                            </option>
                          );
                        })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default AutomobilesAddForm;
