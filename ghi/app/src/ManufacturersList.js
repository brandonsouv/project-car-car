import React from 'react';
import { NavLink } from 'react-router-dom';


function ManufacturersList({manufacturers}) {
  return(
    <>
    <h1>Manufacturers</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>
        {manufacturers.map((manufacturer) => {
            return (
            <tr key={ manufacturer.id }>
                <td className="align-middle">{ manufacturer.name }</td>
            </tr>
            );
        })}
        </tbody>
        </table>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <NavLink to="/manufacturers/new" className="btn btn-primary btn-lg px-4 gap-3">Create a manufacturer</NavLink>
        </div>
    </>
  );
}

export default ManufacturersList;
