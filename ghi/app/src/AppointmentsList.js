import React from 'react';
import { NavLink } from 'react-router-dom';


function AppointmentsList({appointments, setAppointments}) {
  function vipTreatment(is_vip) {
    if (is_vip) {
      return "VIP treatment"
    }
  }
  function cancel(id) {
    fetch(`http://localhost:8080/api/appointments/${id}/`, {
      method:'delete'
    }).then(response => {
      if (response.ok) {
        const updatedAppointments = appointments.filter((appointment) => appointment.id !== id);
        setAppointments(updatedAppointments);
      }});
  };
  function finished(id) {
    const data = {};
    data.is_completed = true;
    const appointmentUrl = `http://localhost:8080/api/appointments/${id}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(appointmentUrl, fetchConfig).then(response => {
        if (response.ok) {
          const updatedAppointments = appointments.filter((appointment) => appointment.id !== id);
          setAppointments(updatedAppointments);
        }});
  };
  return(
      <>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
          <thead>
          <tr>
              <th>VIP Treatment</th>
              <th>VIN</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Cancel / Finished</th>
          </tr>
          </thead>
          <tbody>
            {appointments.filter(appointment => appointment.is_completed===false).map((filteredAppointment) => {
              return (
              <tr key={ filteredAppointment.id }>
                  <td className="align-middle">{ vipTreatment(filteredAppointment.is_vip) }</td>
                  <td className="align-middle">{ filteredAppointment.vin }</td>
                  <td className="align-middle">{ filteredAppointment.customer_name }</td>
                  <td className="align-middle">{ new Date(filteredAppointment.datetime).toLocaleDateString() }</td>
                  <td className="align-middle">{ new Date(filteredAppointment.datetime).toLocaleTimeString() }</td>
                  <td className="align-middle">{ filteredAppointment.technician.name }</td>
                  <td className="align-middle">{ filteredAppointment.reason }</td>
                  <td className="align-middle">
                    <button type="button" className="btn btn-danger" onClick={ () => cancel(filteredAppointment.id) }>Cancel</button>
                    <button type="button" className="btn btn-success" onClick={ () => finished(filteredAppointment.id) }>Finished</button>
                  </td>
              </tr>
              );
            })}
          </tbody>
      </table>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <NavLink to="/appointments/new" className="btn btn-primary btn-lg px-4 gap-3">Make an appointment</NavLink>
          </div>
      </>
    );
  }

export default AppointmentsList;
