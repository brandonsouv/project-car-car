import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import React, {useEffect, useState} from 'react';
import ManufacturersList from './ManufacturersList';
import ManufacturersAddForm from './ManufacturersAddForm';
import VehicleModelsList from './VehicleModelsList';
import VehicleModelsAddForm from './VehicleModelsAddForm';
import AutomobilesList from './AutomobilesList';
import AutomobilesAddForm from './AutomobilesAddForm';
import TechniciansList from './TechniciansList';
import TechniciansAddForm from './TechniciansAddForm';
import AppointmentsList from './AppointmentsList';
import AppointmentsAddForm from './AppointmentsAddForm';
import AppointmentsVINSearchList from './AppointmentsVINSearchList';
import SalesPersonForm from './SalesPersonForm';
import SalesPersonList from './SalesPersonList';
import CustomerForm from './CustomerForm';
import SaleRecordForm from './SaleRecordForm';
import SaleRecordList from './SaleRecordList';


function App() {
  const[manufacturers, setManufacturers] = useState([])
  const getManufacturersData = async () => {
    const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
    const manufacturersResponse = await fetch(manufacturersUrl);
    if (manufacturersResponse.ok) {
      const data = await manufacturersResponse.json();
      setManufacturers(data.manufacturers)
    }
  }
  const[vehicleModels, setVehicleModels] = useState([])
  const getVehicleModelsData = async () => {
    const vehicleModelsUrl = 'http://localhost:8100/api/models/';
    const vehicleModelsResponse = await fetch(vehicleModelsUrl);
    if (vehicleModelsResponse.ok) {
      const data = await vehicleModelsResponse.json();
      setVehicleModels(data.models)
    }
  }
  const[automobiles, setAutomobiles] = useState([])
  const getAutomobilesData = async () => {
    const automobilesUrl = 'http://localhost:8100/api/automobiles/';
    const automobilesResponse = await fetch(automobilesUrl);
    if (automobilesResponse.ok) {
      const data = await automobilesResponse.json();
      setAutomobiles(data.autos)
    }
  }
  const[technicians, setTechnicians] = useState([])
  const getTechniciansData = async () => {
    const techniciansUrl = 'http://localhost:8080/api/technicians/';
    const techniciansResponse = await fetch(techniciansUrl);
    if (techniciansResponse.ok) {
      const data = await techniciansResponse.json();
      setTechnicians(data.technicians)
    }
  }
  const[appointments, setAppointments] = useState([])
  const getAppointmentsData = async () => {
    const appointmentsUrl = 'http://localhost:8080/api/appointments/';
    const appointmentsResponse = await fetch(appointmentsUrl);
    if (appointmentsResponse.ok) {
      const data = await appointmentsResponse.json();
      setAppointments(data.appointments)
    }
  }
  const[salesperson, setSalesPerson] = useState([])
  const getSalesPersonData = async () => {
    const salespersonUrl = 'http://localhost:8090/api/salesperson/';
    const salespersonResponse = await fetch(salespersonUrl);
    if (salespersonResponse.ok) {
      const data = await salespersonResponse.json();
      setSalesPerson(data.sales_person)
    }
  }
  const [customer, setCustomer] = useState([]);
  const getCustomerData = async () => {
    const customerUrl = 'http://localhost:8090/api/customer/';
    const customerResponse = await fetch(customerUrl);
    if (customerResponse.ok) {
      const data = await customerResponse.json();
      setCustomer(data.customer)
    }
  }
  const [saleRecord, setSaleRecord] = useState([]);
  const getSaleRecordData = async () => {
    const saleRecordUrl = 'http://localhost:8090/api/salerecord/';
    const saleRecordResponse = await fetch(saleRecordUrl);
    if (saleRecordResponse.ok) {
      const data = await saleRecordResponse.json();
      setSaleRecord(data.sale_record)
    }
  }
  const[autos, setAutos] = useState([])
  const getAutosData = async () => {
    const autosUrl = 'http://localhost:8090/api/automobiles/';
    const autosResponse = await fetch(autosUrl);
    if (autosResponse.ok) {
      const data = await autosResponse.json();
      setAutos(data.autos)
    }
  }
  useEffect(() => {getManufacturersData(); getVehicleModelsData(); getAutomobilesData(); getTechniciansData();
    getAppointmentsData(); getSalesPersonData(); getCustomerData(); getSaleRecordData(); getAutosData();
  }, []);
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
              <Route path="" element={<ManufacturersList manufacturers={manufacturers} />} />
              <Route path="new" element={<ManufacturersAddForm getManufacturersData={getManufacturersData} />} />
          </Route>
          <Route path="vehicle_models">
              <Route path="" element={<VehicleModelsList vehicleModels={vehicleModels} />} />
              <Route path="new" element={<VehicleModelsAddForm getVehicleModelsData={getVehicleModelsData} manufacturers={manufacturers} />} />
          </Route>
          <Route path="automobiles">
              <Route path="" element={<AutomobilesList automobiles={automobiles} />} />
              <Route path="new" element={<AutomobilesAddForm getAutomobilesData={getAutomobilesData} vehicleModels={vehicleModels} />} />
          </Route>
          <Route path="technicians">
              <Route path="" element={<TechniciansList technicians={technicians} />} />
              <Route path="new" element={<TechniciansAddForm getTechniciansData={getTechniciansData} />} />
          </Route>
          <Route path="appointments">
              <Route path="" element={<AppointmentsList appointments={appointments} setAppointments={setAppointments} />} />
              <Route path="new" element={<AppointmentsAddForm getAppointmentsData={getAppointmentsData} technicians={technicians} />} />
              <Route path="search" element={<AppointmentsVINSearchList appointments={appointments} />} />
          </Route>
          <Route path="salesperson">
              <Route path="" element={<SalesPersonList salesperson={salesperson}  />} />
              <Route path="new" element={<SalesPersonForm sales_person={salesperson} getSalesPersonData={getSalesPersonData} getAutosData={getAutosData} />} />
          </Route>
          <Route path="customer">
              <Route path="" element={<CustomerForm customer={customer} getCustomerData={getCustomerData} />} />
          </Route>
          <Route path="salerecord">
              <Route path="" element={<SaleRecordList salesperson={salesperson} saleRecord={saleRecord} />} />
              <Route path="new" element={<SaleRecordForm getSaleRecordData={getSaleRecordData} salesperson={salesperson} customer={customer} autos={autos} getAutosData={getAutosData} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
