import React, {useState} from 'react';
import { NavLink } from 'react-router-dom';


function SaleRecordList({salesperson, saleRecord}) {
  const [searchSalesPerson, setSearchSalesPerson] = useState('');
  const handleSearchSalesPersonChange = (event) => {
    const value = event.target.value;
    setSearchSalesPerson(value);
  }
  function searchResultFilter (prop) {
    if (searchSalesPerson === '') {
      return prop
    } else {
      if (prop.sales_person.id === Number.parseInt(searchSalesPerson)) {
        return prop
      }
    }
  }
  return(
    <>
    <br />
    <div className="d-flex">
      <select onChange={handleSearchSalesPersonChange} value={searchSalesPerson} id="salesperson" name="salesperson" className="form-select">
        <option value="">Choose a sales person</option>
        {salesperson.map((sales) => {
                return (
                  <option value={sales.id} key={sales.id}>
                    {sales.name} (Employee Number: {sales.employee_number})
                  </option>
                );
              })}
      </select>
    </div>
    <br />
    <h1>Sale Record</h1>
    <table className="table table-striped">
      <thead>
        <tr>
            <th>Sales Person</th>
            <th>Employee Number</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale Price</th>
        </tr>
      </thead>
      <tbody>
        {saleRecord.filter(searchResultFilter).map((filteredrecord) => {
          return (
            <tr key={ filteredrecord.id }>
                <td className="align-middle">{ filteredrecord.sales_person.name }</td>
                <td className="align-middle">{ filteredrecord.sales_person.employee_number }</td>
                <td className="align-middle">{ filteredrecord.customer.name }</td>
                <td className="align-middle">{ filteredrecord.automobile.vin }</td>
                <td className="align-middle">{ filteredrecord.sale_price }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
      <NavLink to="/salerecord/new" className="btn btn-primary btn-lg px-4 gap-3">Create Sale Record</NavLink>
    </div>
    </>
    );
  }

export default SaleRecordList;
