import React, {useState, useEffect} from 'react';


function AppointmentsVINSearchList({appointments}) {
  const [searchedVIN, setSearchedVIN] = useState('');
  const [searchedResult, setSearchResult] = useState([]);
  useEffect(() => { setSearchResult(appointments); }, [appointments]);
  const searchVIN = (event) => {
    const value = event.target.value;
    setSearchedVIN(value);
  }
  function handleSearch() {
    if (searchedVIN !== "") {
      const filteredAppointments = appointments.filter(appointment => appointment.vin === searchedVIN);
      setSearchResult(filteredAppointments);
    } else {
      setSearchResult(appointments)
    }
  }
  function vipTreatment(is_vip) {
    if (is_vip) {
      return "VIP treatment"
    }
  }
  function finished(is_completed) {
    if (is_completed) {
      return "finished"
    } else {
      return "Not finished"
    }
  }
  return(
    <>
    <br />
    <div className="d-flex">
      <input className="form-control me-2" onChange={searchVIN} value={searchedVIN} type="search" placeholder="Search VIN" aria-label="Search" />
      <button className="btn btn-outline-success me-2" type="button" onClick={handleSearch} >Search VIN</button>
    </div>
    <br />
    <h1>Service Appointments</h1>
    <table className="table table-striped">
        <thead>
        <tr>
            <th>VIP Treatment</th>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Finished</th>
        </tr>
        </thead>
        <tbody>
          {searchedResult.map((appointment) => {
            return (
            <tr key={ appointment.id }>
                <td className="align-middle">{ vipTreatment(appointment.is_vip) }</td>
                <td className="align-middle">{ appointment.vin }</td>
                <td className="align-middle">{ appointment.customer_name }</td>
                <td className="align-middle">{ new Date(appointment.datetime).toLocaleDateString() }</td>
                <td className="align-middle">{ new Date(appointment.datetime).toLocaleTimeString() }</td>
                <td className="align-middle">{ appointment.technician.name }</td>
                <td className="align-middle">{ appointment.reason }</td>
                <td className="align-middle">{ finished(appointment.is_completed) }</td>
            </tr>
            );
          })}
        </tbody>
    </table>
    </>
    );
  }

export default AppointmentsVINSearchList;
