# CarCar

CarCar is an application made to help manage a automobile dealership business.
The application consists of three microservices: Inventory, Sales, and Service connected through VO models with the Sales and Service, leading back to the Inventory.

## Team:

* John Liu - Service Microservice
* Brandon Souvannarath - Sales Microservice

## Microservices:

The Inventory microservice handles the list and enter for manufacturers, vehicle models, and automobile information.

The Sales microservice handles (1) the list and enter for sales person and customer information, and (2) enter and list for sale recordlist (with search function for sales person).

The Service microservice handles (1) the list and enter for technicians, and (2) the list for appointments not yet serviced with cancellation function and the search for service appointments through car's VIN number.

## Diagram:

![browser](./images/diagram.png)

## Models/Fields:

* Inventory:<br>
    Manufacturer: name<br>
    VehicleModel: name, picture_url, manufaturer (ForeignKey)<br>
    Automobile: color, year, vin, model (ForeignKey)<br>

* Sales:<br>
    AutomobileVO (Value Object): imported_href, vin, is_sole<br>
	  Customer: name, address, phone_number<br>
	  SalesPerson: name, employee_number<br>
	  SaleRecord: sale_price, customer (ForeignKey), sales_person (ForeignKey), automobile (ForeignKey)<br>

* Service:<br>
    AutomobileVO (Value Object): vin<br>
	  technician: name, employee_number<br>
	  Appointment: customer_name, vin, reason, datetime, technician (ForeignKey), is_vip, is_completed<br>

## Deployment:

To deploy this project:
1. Fork [this project](https://gitlab.com/zodramleo/project-car-car).
2. Copy the clone with https link.
3. Get your docker ready (docker can be donloaded from https://www.docker.com/).
4. In your terminal, cd to your project directory and run the following commands:

```bash
git clone <<http link>>
docker volume create beta-data
docker-compose build
docker-compose up
```

5. Wait for all containers, images, volumes to build. The application should show up on your [browser](http://localhost:3000) at http://localhost:3000.

## Ports
3000 - Connect to React app<br>
15432 - Postgres DB<br>
8080 - Service api<br>
8090 - Sales api<br>
8100 - Inventory api

## Front-End URL Reference:

### Inventory

|              Action              |                     URL                        |
| :------------------------------- | :--------------------------------------------- |
| `List manufacturers` 			   | `http://localhost:3000/manufacturers`          |
| `Create a manufacturer`          | `http://localhost:3000/manufacturers/new`      |
| `List vehicle models` 		   | `http://localhost:3000/vehicle_models`         |
| `Create a vehicle model`         | `http://localhost:3000/vehicle_models/new`     |
| `List automobiles` 			   | `http://localhost:3000/automobiles`            |
| `Add an automobile`              | `http://localhost:3000/automobiles/new`        |

### Sales

|              Action              |                     URL                        |
| :------------------------------- | :--------------------------------------------- |
| `List sales person` 			   | `http://localhost:3000/salesperson`            |
| `Add a sales person`             | `http://localhost:3000/salesperson/new`     |
| `Enter and list customer`        | `http://localhost:3000/customer`     |
| `List sale record (with search via sales person)`   | `http://localhost:3000/salerecord`            |
| `Create a sale record`          | `http://localhost:3000/salerecord/new`     |

### Service

|              Action                   |                     URL                        |
| :------------------------------------ | :--------------------------------------------- |
| `List technicians` 			        | `http://localhost:3000/technicians`            |
| `Add a technician`                    | `http://localhost:3000/technicians/new`        |
| `List service appointments`	        | `http://localhost:3000/appointments`           |
| `Add a service appointments`          | `http://localhost:3000/appointments/new`       |
| `Search service appointments (via VIN)` | `http://localhost:3000/appointments/search`    |

## Back-End API Reference:

### Inventory API Reference

#### Manufacturers

```http
/api/manufacturers
```

|              Action              |  Method  |                     URL                        |
| :------------------------------- | :------- | :--------------------------------------------- |
| `List manufacturers` 			   | `GET`    | `http://localhost:8100/api/manufacturers/`     |
| `Create a manufacturer`          | `POST`   | `http://localhost:8100/api/manufacturers/`     |
| `Get a specific manufacturer`    | `GET`    | `http://localhost:8100/api/manufacturers/:id/` |
| `Update a specific manufacturer` | `PUT`    | `http://localhost:8100/api/manufacturers/:id/` |
| `Delete a specific manufacturer` | `DELETE` | `http://localhost:8100/api/manufacturers/:id/` |

#### To test the MANUFACTURER API in Insomnia:
*List manufacturers* with a **GET** request to -> http://localhost:8100/api/manufacturers/<br>
*Create a manufacturer* with a **POST** request to -> http://localhost:8100/api/manufacturers/ with this JSON:<br>
`{
  "name": "Chrysler"
}`

#### Vehicle Models

```http
/api/models
```

|              Action               |  Method  |                    URL                  |
| :-------------------------------- | :------- | :-------------------------------------- |
| `List vehicle models`             | `GET`    | `http://localhost:8100/api/models/`     |
| `Create a vehicle model`          | `POST`   | `http://localhost:8100/api/models/`     |
| `Get a specific vehicle model`    | `GET`    | `http://localhost:8100/api/models/:id/` |
| `Update a specific vehicle model` | `PUT`    | `http://localhost:8100/api/models/:id/` |
| `Delete a specific vehicle model` | `DELETE` | `http://localhost:8100/api/models/:id/` |

#### To test the VEHICLEMODEL API in Insomnia:
*List vehicle models* with a **GET** request to -> http://localhost:8100/api/models/<br>
*Create a vechicle model* with a **POST** request to -> http://localhost:8100/api/models/ with this JSON:<br>
`{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}`


#### Automobile

```http
/api/automobiles
```

|             Action             |  Method  |                     URL                       |
| :----------------------------- | :------- | :-------------------------------------------- |
| `List automobiles`             | `GET`    | `http://localhost:8100/api/automobiles/`      |
| `Create an automobile`         | `POST`   | `http://localhost:8100/api/automobiles/`      |
| `Get a specific automobiles`   | `GET`    | `http://localhost:8100/api/automobiles/:vin/` |
| `Update a specific automobile` | `PUT`    | `http://localhost:8100/api/automobiles/:vin/` |
| `Delete a specific automobile` | `DELETE` | `http://localhost:8100/api/automobiles/:vin/` |

#### To test the AUTOMOBILE API in Insomnia:
*List automobiles* with a **GET** request to -> http://localhost:8100/api/automobiles/<br>
*Create an automobile* with a **POST** request to -> http://localhost:8100/api/automobiles/ with this JSON:<br>
`{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}`


### Sales API Reference

#### Sales Person

```http
/api/sales_person
```

| Action | Method     | URL                |
| :-------- | :------- | :------------------------- |
| `List sales person` | `GET` | `http://localhost:8090/api/salesperson/` |
| `Create a sales person` | `POST` | `http://localhost:8090/api/salesperson/` |

#### To test the SALESRECORD API in Insomnia:
*List sales person* with a **GET** request to -> http://localhost:8090/api/salesperson/ <br>
*Create a sales person* with a **POST** request to -> http://localhost:8090/api/salesperson/ with this JSON:<br>
`{
    "name": "Negotiat'n Nancy",
	"employee_number": "007"
}` <br>

#### Customer

```http
/api/customers
```

| Action | Method     | URL                       |
| :-------- | :------- | :-------------------------------- |
| `List customers` | `GET` | `http://localhost:8090/api/customer/` |
| `Add a customer` | `POST` | `http://localhost:8090/api/customer/` |

#### To test the SALESRECORD API in Insomnia:
*List customers* with a **GET** request to -> http://localhost:8090/api/customer/<br>
*Add a customer* with a **POST** request to -> http://localhost:8090/api/customer/ with this JSON:<br>
`{
	"name": "Tirekickin Tony",
	"address": "616 Maybe Ln.",
	"phone_number": "984654324"
}`<br>

#### Automobile

```http
/api/automobiles
```

| Action | Method     | URL                       |
| :-------- | :------- | :-------------------------------- |
| `List automobiles` | `GET` | `http://localhost:8090/api/automobiles/` |


#### To test the SALESRECORD API in Insomnia:
*List automobiles* with a **GET** request to -> http://localhost:8090/api/automobiles/<br>


#### Sales Record

```http
/api/sales
```

| Action | Method     | URL                       |
| :-------- | :------- | :-------------------------------- |
| `List all sale records` | `GET` | `http://localhost:8090/api/salerecord/` |
| `Add a sale record` | `POST` | `http://localhost:8090/api/salerecord/` |

#### To test the SALESRECORD API in Insomnia:
*List all sale records* with a **GET** request to -> http://localhost:8090/api/salerecord/<br>
*Add a sale record* with a **POST** request to -> http://localhost:8090/api/salerecord/ with this JSON:<br>
`{
        "sale_price": 40000,
        "customer": 2,
        "sales_person": 4,
        "automobile": "oeialkfjlk5431f35"
    }`
<br>



### Service API Reference

#### Technician

```http
/api/technicians
```

| Action                | Method  | URL                                      |
| :-------------------- | :------ | :--------------------------------------- |
| `List technicians`    | `GET`   | `http://localhost:8080/api/technicians/` |
| `Create a technician` | `POST`  | `http://localhost:8080/api/technicians/` |

#### To test the TECHNICIAN API in Insomnia:
*List technicians* with a **GET** request to -> http://localhost:8080/api/technicians/<br>
*Create a technician* with a ***POST** request to -> http://localhost:8080/api/technicians/ with this JSON:<br>
`{
	"id": 1,
	"name": "Jay",
	"employee_number": 1234
}`

#### Appointment

```http
/api/appointments
```

|    Action                       | Method   | URL                                           |
| :------------------------------ | :------- | :-------------------------------------------- |
| `List appointments`             | `GET`    | `http://localhost:8080/api/appointments/`     |
| `Create an appointment`         | `POST`   | `http://localhost:8080/api/appointments/`     |
| `Update a specific appointment` | `PUT`    | `http://localhost:8080/api/appointments/:id/` |
| `Delete a specific appointment` | `DELETE` | `http://localhost:8080/api/appointments/:id/` |

#### To test the APPOINTMENT API in Insomnia:
*List appointments* with a **GET** request to -> http://localhost:8080/api/appointments/<br>
*Create an appointment* with a **POST** request to -> http://localhost:8080/api/appointments/ with this JSON:<br>
`{
	"customer_name": "Mark",
	"datetime":"2023-01-27T09:9:00.047930+00:00",
	"vin": "1C3CC5FB2AN120174",
	"reason": "Wheel alignment",
	"technician": "Jay"
}`

*Update a specific appointment* with a **PUT** request to -> http://localhost:8080/api/appointments/:id/<br>
*Delete a specific appointment* with a **DELETE** request to -> http://localhost:8080/api/appointments/:id/<br>
